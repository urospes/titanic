import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    #output = []

    df['Name'] = df['Name'].map(lambda name: name.split()[0])
    df_median = df.groupby(['Name'])['Age'].median()
    df_sum = df.Age.isnull().groupby(['Name']).sum()

    df.groupby(['Name'])['Age'].apply(lambda age: age.fillna(age.median()))

    return [('Mr.', df_sum[0][-1], df_median[1][-1]), ('Mrs.', df_sum[1][-1], df_median[0][-1]) , ('Miss.', df_sum[2][-1], df_median[2][-1])]
